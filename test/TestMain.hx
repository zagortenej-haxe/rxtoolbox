// Copyright 2024, Ravendyne Inc
// SPDX-License-Identifier: MIT

package;

// import utest.Runner;
// import utest.ui.Report;
import utest.UTest;


class TestMain {

  public static function main() {

    UTest.run([
      new TestSmoke(),
      new TestProcRun(),
      new TestJsonConverter(),
      new TestProducerConsumer(),
      new TestProducerOperators(),
    ]);
  }
}
