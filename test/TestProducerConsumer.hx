/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package;

import utest.Async;
import rxtoolbox.rxhx.Producer;
import utest.Assert;
import utest.Test;

import rxtoolbox.rxhx.Consumer;

using rxtoolbox.json.JsonConverter;

class TestProducerConsumer extends Test {

  function testConsumerSmoke() {

    var obs = new Consumer<String>({
    });
    obs.next( 'bar' );
    obs.error( 'foo' );


    obs = new Consumer<String>({
    });
    obs.next( 'baz' );
    obs.complete();

    Assert.notNull( obs );
  }

  function testConsumerNextComplete() {

    var r : String = '';
    var c : String = '';

    var obs = new Consumer<String>({
        next: v -> r = v,
        complete: () -> c = 'c',
    });

    obs.next( 'bar' );
    Assert.equals( 'bar', r );
    Assert.equals( '', c );

    obs.next( 'foo' );
    Assert.equals( 'foo', r );
    Assert.equals( '', c );

    obs.complete();

    obs.next( 'baz' );
    Assert.equals( 'foo', r );
    Assert.equals( 'c', c );
  }

  function testConsumerError() {

    var r : String = '';
    var c : String = '';

    var obs = new Consumer<String>({
        next: v -> r = v,
        error: e -> r = e,
        complete: () -> c = 'c',
    });

    obs.next( 'bar' );
    Assert.equals( 'bar', r );
    Assert.equals( '', c );

    obs.error( 'foo' );
    Assert.equals( 'foo', r );
    Assert.equals( '', c );

    obs.next( 'baz' );
    Assert.equals( 'foo', r );
    Assert.equals( '', c );
  }

  function testProducer( async : Async ) {
    var r : String = '';
    var c : String = '';

    var producer = new Producer( consumer -> {
      // this is logic for producer,
      // it should produce values,
      // notify consumer of the new value
      // by calling its next() method,
      // and call consumer's complete()
      // method when it's done,
      // or error() when there's been an error

      consumer.next('foo');
      consumer.complete();

    }, () -> {
      // a Void->Void function which will be called
      //  when a subscriber unsubscribes
      c = 'u';
    });

    var subscription = producer.subscribe({
      next: v -> r = v,
      complete: () -> {
        Assert.equals( 'foo', r );
        // complete() should unsubscribe after handler is done
        Assert.equals( '', c );
        async.done();
      }
    });
  }

  function testProducerOf( async : Async ) {

    var producer = Producer.of('foo');

    producer.subscribe({
      next: v -> {
        Assert.equals( 'foo', v );
        async.done();
      },
    });
  }

  function testProducerFrom( async : Async ) {
    var r : Array<String> = [];

    var producer = Producer.from(['foo','bar','baz']);

    producer.subscribe({
      next: v -> r.push( v ),
      complete: () -> {
        Assert.same( ['foo','bar','baz'], r );
        async.done();
      }
    });
  }

  // @:timeout(5000) //change timeout (default: 250ms)
  function testProducerInterval( async : Async ) {
    var cnt = 0;

    var sub = Producer
    .interval( 10, () -> '' )
    .subscribe({
      next: v -> {
        cnt++;
      },
    });

    haxe.Timer.delay( () -> {
      sub.unsubscribe();
      Assert.equals( 3, cnt );
      async.done();
    }, 35 );
  }
}
