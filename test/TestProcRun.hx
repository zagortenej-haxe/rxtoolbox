/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package;

import rxtoolbox.cli.ProcRun;
import utest.Assert;
import utest.Test;

using rxtoolbox.json.JsonConverter;

class TestProcRun extends Test {

  function testIt() {
    var r = ProcRun.cmd("cat",["haxelib.json"]);
    Assert.equals( 0, r.status );
    Assert.notNull( r.text );
    Assert.notNull( r.json );
    // trace(r.text);

    r = ProcRun.cmd("ls");
    Assert.equals( 0, r.status );
    Assert.notNull( r.text );
    Assert.isNull( r.json );
    // trace(r.text);
  }
}
