/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package;

import haxe.Json;

import utest.Assert;
import utest.Test;

using rxtoolbox.json.JsonConverter;

class TestJsonConverter extends Test {

  function testSmoke() {
    var jt = JsonConverter.convert(Json.parse('{}'));
    Assert.notNull( jt );
  }

  function testIs() {
    var jt = JsonConverter.convert(Json.parse('
    {
      "val_arr": [
        "strg",
        42,
        32.1,
        false
      ],
      "val_str": "bar value",
      "val_double": 23.45,
      "val_int": 23,
      "val_bool": false,
      "val_null": null
    }
    '));

    Assert.isFalse( jt.isArray() );
    Assert.isFalse( jt.isString() );
    Assert.isTrue( jt.isObject() );
    Assert.isFalse( jt.isNumeric() );
    Assert.isFalse( jt.isDouble() );
    Assert.isFalse( jt.isInteger() );
    Assert.isFalse( jt.isBoolean() );
    Assert.isFalse( jt.isNull() );

    var obj = jt.getObject();
    Assert.isTrue( obj.exists('val_arr') );
    Assert.isTrue( obj.exists('val_str') );
    Assert.isTrue( obj.exists('val_double') );
    Assert.isTrue( obj.exists('val_int') );
    Assert.isTrue( obj.exists('val_bool') );
    Assert.isTrue( obj.exists('val_null') );

    var val_arr = obj.get('val_arr');
    Assert.isTrue( val_arr.isArray() );
    Assert.isFalse( val_arr.isString() );
    Assert.isFalse( val_arr.isObject() );
    Assert.isFalse( val_arr.isNumeric() );
    Assert.isFalse( val_arr.isDouble() );
    Assert.isFalse( val_arr.isInteger() );
    Assert.isFalse( val_arr.isBoolean() );
    Assert.isFalse( val_arr.isNull() );
    var jt_arr = val_arr.getArray();
    Assert.equals( 4, jt_arr.length );
    Assert.isTrue( jt_arr[0].isString() );
    Assert.isTrue( jt_arr[1].isInteger() );
    Assert.isTrue( jt_arr[2].isDouble() );
    Assert.isTrue( jt_arr[3].isBoolean() );

    var val_str = obj.get('val_str');
    Assert.isFalse( val_str.isArray() );
    Assert.isTrue( val_str.isString() );
    Assert.isFalse( val_str.isObject() );
    Assert.isFalse( val_str.isNumeric() );
    Assert.isFalse( val_str.isDouble() );
    Assert.isFalse( val_str.isInteger() );
    Assert.isFalse( val_str.isBoolean() );
    Assert.isFalse( val_str.isNull() );
    Assert.equals( "bar value", val_str.getString() );

    var val_double = obj.get('val_double');
    Assert.isFalse( val_double.isArray() );
    Assert.isFalse( val_double.isString() );
    Assert.isFalse( val_double.isObject() );
    Assert.isTrue( val_double.isNumeric() );
    Assert.isTrue( val_double.isDouble() );
    Assert.isFalse( val_double.isInteger() );
    Assert.isFalse( val_double.isBoolean() );
    Assert.isFalse( val_double.isNull() );
    Assert.equals( 23.45, val_double.getDouble() );

    var val_int = obj.get('val_int');
    Assert.isFalse( val_int.isArray() );
    Assert.isFalse( val_int.isString() );
    Assert.isFalse( val_int.isObject() );
    Assert.isTrue( val_int.isNumeric() );
    Assert.isTrue( val_int.isDouble() );
    Assert.isTrue( val_int.isInteger() );
    Assert.isFalse( val_int.isBoolean() );
    Assert.isFalse( val_int.isNull() );
    Assert.equals( 23, val_int.getInteger() );

    var val_bool = obj.get('val_bool');
    Assert.isFalse( val_bool.isArray() );
    Assert.isFalse( val_bool.isString() );
    Assert.isFalse( val_bool.isObject() );
    Assert.isFalse( val_bool.isNumeric() );
    Assert.isFalse( val_bool.isDouble() );
    Assert.isFalse( val_bool.isInteger() );
    Assert.isTrue( val_bool.isBoolean() );
    Assert.isFalse( val_bool.isNull() );
    Assert.equals( false, val_bool.getBoolean() );

    var val_null = obj.get('val_null');
    Assert.isFalse( val_null.isArray() );
    Assert.isFalse( val_null.isString() );
    Assert.isFalse( val_null.isObject() );
    Assert.isFalse( val_null.isNumeric() );
    Assert.isFalse( val_null.isDouble() );
    Assert.isFalse( val_null.isInteger() );
    Assert.isFalse( val_null.isBoolean() );
    Assert.isTrue( val_null.isNull() );

  }

  function testConversions() {
    var jt = JsonConverter.convert(Json.parse('
    {
      "foo_map": {
        "foo_str": "bar value",
        "foo_n": 23
      },
      "bar_arr": [
        "strg",
        42,
        32.1,
        [1,2,3],
        false
      ],
      "null_val": null
    }
    '));

    Assert.isFalse( jt.isArray() );
    Assert.isFalse( jt.isString() );
    Assert.isTrue( jt.isObject() );
    Assert.isFalse( jt.isDouble() );
    Assert.isFalse( jt.isInteger() );
    Assert.isFalse( jt.isBoolean() );
    Assert.isFalse( jt.isNull() );

    var obj = jt.getObject();
    Assert.isTrue( obj.exists('foo_map') );
    Assert.isTrue( obj.exists('bar_arr') );
    Assert.isTrue( obj.exists('null_val') );

    var foo_map = obj.get('foo_map');
    Assert.isTrue( foo_map.isObject() );
    var foo_map_obj = foo_map.getObject();
    Assert.isTrue( foo_map_obj.exists('foo_str') );
    Assert.isTrue( foo_map_obj.exists('foo_n') );

    var foo_str = foo_map_obj.get('foo_str');
    Assert.isTrue( foo_str.isString() );
    Assert.equals( "bar value", foo_str.getString() );

    // var foo_n = foo_map_obj.get('foo_n');
    // Assert.isTrue( foo_n.isInteger() );
    // Assert.equals( 23, foo_n.getNumber() );
  }
}