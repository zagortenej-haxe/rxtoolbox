/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package;

import utest.Async;
import utest.Assert;
import utest.Test;

import rxtoolbox.rxhx.Producer;

using rxtoolbox.rxhx.ProducerOperators;
using rxtoolbox.json.JsonConverter;

class TestProducerOperators extends Test {

  function testTake( async : Async ) {

    var count = 0;
    Producer
    .from([9,8,7,6,5,4,3,2,1])
    .take(4)
    .subscribe({
      next: (_) -> count++,
      complete: () -> {
        Assert.equals( 4, count );
        async.done();
      }
    });
  }

  function testCollect( async : Async ) {

    var count = 0;
    var ar = null;
    Producer
    .from([9,8,7,6,5,4,3,2,1])
    .collect()
    .subscribe({
      next: (r) -> {
        count++;
        ar = r;
      },
      complete: () -> {
        Assert.equals( 1, count );
        Assert.notNull( ar );
        Assert.equals( 9, ar.length );
        Assert.equals( 9, ar[0] );
        Assert.equals( 1, ar[8] );
        async.done();
      }
    });
  }

  function testMap( async : Async ) {

    var val = 0;
    Producer
    .of(4)
    .map( v -> 2 * v )
    .subscribe({
      next: (v) -> val = v,
      complete: () -> {
        Assert.equals( 8, val );
        async.done();
      }
    });
  }

  function testSwitchMap( async : Async ) {

    var val = 0;
    Producer
    .of(4)
    .switchMap( v -> Producer.of(2 * v) )
    .subscribe({
      next: v -> {
        val = v;
        // trace('outer next', v);
      },
      complete: () -> {
        Assert.equals( 8, val );
        async.done();
      }
    });
  }

  function testSwitchMap2( async : Async ) {

    var val = 0;
    Producer
    .of(4)
    .switchMap( v -> Producer.of(2 * v) )
    .switchMap( v -> Producer.of(2 * v) )
    .subscribe({
      next: v -> {
        val = v;
      },
      complete: () -> {
        Assert.equals( 16, val );
        async.done();
      }
    });
  }

  function testSwitchMapMap( async : Async ) {

    var val = 0;
    Producer
    .of(4)
    .switchMap( v -> Producer.of(2 * v) )
    .map( v -> v * 3 )
    .subscribe({
      next: v -> {
        val = v;
        // trace('outer next', v);
      },
      complete: () -> {
        Assert.equals( 24, val );
        async.done();
      }
    });
  }

  function testSwitchMapSwitchMap( async : Async ) {

    var val = 0;
    Producer
    .of(1)
    .switchMap( v -> Producer.from([2 * v, 3 * v]) )
    .switchMap( v -> Producer.from([4 * v, 5 * v]) )
    .subscribe({
      next: v -> {
        val += v;
        // trace('outer next', v);
      },
      complete: () -> {
        Assert.equals( 45, val );
        async.done();
      }
    });
  }

  function testDelay( async : Async) {
    var val = 0;
    Producer.of(42).delay(10).subscribe({
      next: v -> val = v,
    });
    haxe.Timer.delay(() -> {
      Assert.equals( 0, val);
    }, 5);
    haxe.Timer.delay(() -> {
      Assert.equals( 42, val);
      async.done();
    }, 15);
  }

  function testSequenceOfOperations( async : Async) {
    var flag = 0;
    haxe.Timer.delay(() -> {
      // trace('you');
      flag++;
      Assert.equals( 2, flag );
      async.done();
    },0);
    // trace('me');
    flag++;
  }
}
