package;

import rxtoolbox.cli.TerminalLog;
import sys.thread.Thread;
import haxe.Timer;
import haxe.Json;

import rxtoolbox.rxhx.Producer;
import rxtoolbox.cli.ArgsCollector;

using rxtoolbox.json.JsonConverter;
using rxtoolbox.rxhx.ProducerOperators;

class RxTools {
  public static function main() {

    trace(Std.isOfType(null, null));

    var ac = new ArgsCollector(Sys.args(),[
      'flag-foo',
      'json',
      'obs',
      'evt',
      'log',
    ]);

    trace(ac.params);
    trace(ac.values);

    if( ! ac.params.exists("in") ) {
      Sys.println("Missing '--in' parameter");
      return;
    }

    var foo_flag = ac.params.exists('flag-foo');

    trace('foo_flag = ${foo_flag}');
    trace('in = "${ac.params.get('in')}"');


    if( ac.params.exists("json") ) {
      json();
      return;
    }

    if( ac.params.exists("obs") ) {
      producers();
      return;
    }

    if( ac.params.exists("log") ) {
      log();
      return;
    }

    if( ac.params.exists("evt") ) {
      testTake();
      var lock = new sys.thread.Lock();
      Thread.create(() -> {
        testTake();
        lock.release();
      });
      lock.wait();
      // haxe.Timer.delay(() -> trace('YO!'), 5000);
      return;
    }
  }

  static function testTake() {
    trace('test take');
    var count = 0;
    Producer
    .from([9,8,7,6,5,4,3,2,1])
    .take(4)
    .subscribe({
      next: (_) -> {
        count++;
        trace(count);
      },
      complete: () -> {
        if( count != 4 ) throw 'count is NOT 4!';
        else trace('count is correct!');
      }
    });
  }


  static function producers() {
    trace('producers demo');
    var obs = Producer.interval( 1000, () -> 'tick' );
    var sub = obs.subscribe({
      next: v -> trace(v),
    });
    Timer.delay( sub.unsubscribe, 3500 );

    var cnt = 0;
    var obs = Producer.interval( 1000, () -> { cnt++; return cnt; } );
    var sub = obs
    .map( v -> 2 * v )
    .subscribe({
      next: v -> trace(v),
    });
    Timer.delay( sub.unsubscribe, 3500 );

    Producer.of('tock').delay( 4000 ).subscribe({
      next: v -> trace(v),
    });

    Producer.empty().delay( 5000 ).subscribe({
      next: _ -> trace('fini'),
    });
  }

  static function json() {
    trace('json demo');
    var jt = JsonConverter.convert(Json.parse('
    {
      "foo_map": {
        "foo_str": "bar value",
        "foo_n": 23
      },
      "bar_arr": [
        "strg",
        42,
        32.1,
        [1,2,3]
      ],
      "null_val": null
    }
    '));

    Sys.println('isArray = ${jt.isArray()}');
    Sys.println('isString = ${jt.isString()}');
    Sys.println('isObject = ${jt.isObject()}');
    Sys.println('isDouble = ${jt.isDouble()}');
    Sys.println('isInteger = ${jt.isInteger()}');
    Sys.println('isBoolean = ${jt.isBoolean()}');
    Sys.println('isNull = ${jt.isNull()}');

    var obj = jt.getObject();
    Sys.println('foo_map ? ${obj.exists('foo_map')}');
    Sys.println('bar_arr ? ${obj.exists('bar_arr')}');
    Sys.println('null_val ? ${obj.exists('null_val')}');

    var arr = obj.get('bar_arr').getArray();
    Sys.println( arr[0].getString() );
  }

  static function log() {
    trace('log demo');
    var tlog = new TerminalLog();
    tlog.message('just a message');
    tlog.info('info message');
    tlog.warn('warn message');
    tlog.debug('debug message');
    tlog.error('error message');
    tlog.message(
      TerminalLog.BLUE + 'BLUE ' + 
      TerminalLog.CYAN + 'CYAN ' + 
      TerminalLog.GREEN + 'GREEN ' + 
      TerminalLog.MAGENTA + 'MAGENTA ' + 
      TerminalLog.RED + 'RED ' + 
      TerminalLog.WHITE + 'WHITE ' + 
      TerminalLog.YELLOW + 'YELLOW ' + 
      TerminalLog.RESET + TerminalLog.ITALIC + 'ITALIC ' + 
      TerminalLog.RESET + TerminalLog.BOLD + 'BOLD ' + 
      TerminalLog.RESET
    );
  }
}
