/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package rxtoolbox.json;

import rxtoolbox.exception.UnsupportedOperation;
import rxtoolbox.exception.InvalidOperation;
import haxe.DynamicAccess;
import haxe.ds.StringMap;


enum JsonType {
  JTArray(v:Array<JsonType>);
  JTString(v:String);
  JTObject(v:StringMap<JsonType>);
  JTDouble(v:Float);
  JTInteger(v:Int);
  JTBoolean(v:Bool);
  JTNull;
  JTUnsupported;
}

private enum abstract JT(Int) {
  var JT_Array;
  var JT_String;
  var JT_Map;
  var JT_Double;
  var JT_Integer;
  var JT_Boolean;
  var JT_Null;
  var JT_Unsupported;
}

/**
  Converts `Dynamic` produced by `Json.parse` into typed enums.

  Because it's easier to work with.

  You could also use `DynamicAccess<Dynamic>` and a lot of `Std.isOfType( value, StringMap/Array/String/Float/Int/Bool )`
  and implicit casts like `var cm : StringMap<Dynamic> = value;`, etc..

  In part very loosely inspired by `com.google.gson.JsonElement`.
**/
final class JsonConverter {
  private function new() {}

  static function getType( v : Dynamic ) : JT {
    if( null == v ) return JT_Null;
    if( Std.isOfType( v, Array) ) return JT_Array;
    else if( Std.isOfType( v, String) ) return JT_String;
    else if( Reflect.isObject( v ) ) return JT_Map;
    else if( Std.isOfType( v, Float ) ) return JT_Double;
    else if( Std.isOfType( v, Int ) ) return JT_Integer;
    else if( Std.isOfType( v, Bool ) ) return JT_Boolean;
    return JT_Unsupported;
  }

  static function convert_map( v : DynamicAccess<Dynamic> ): StringMap<JsonType> {
    var m: StringMap<JsonType> = new StringMap<JsonType>();
    for( key => value in v ) {
      m.set( key, convert( value ) );
    }
    return m;
  }

  static function convert_array( v : Dynamic ): Array<JsonType> {
    var m: Array<JsonType> = [];
    var va: Array<Any> = cast v;
    for( value in va ) {
      m.push( convert( value ) );
    }
    return m;
  }

  public static function convert( v : Dynamic ): JsonType {

    switch( getType( v ) ) {

      case JT_Array:
        return JTArray( convert_array( v ) );

      case JT_String:
        return JTString( Std.string( v ) );

      case JT_Map:
        return JTObject( convert_map( v ) );

      case JT_Double:
        return JTDouble( cast(v, Float) );

      case JT_Integer:
        return JTInteger( cast(v, Int) );

      case JT_Boolean:
        return JTBoolean( cast(v, Bool) );

      case JT_Null:
        return JTNull;

      case JT_Unsupported:
        throw new UnsupportedOperation('Unsupported conversion for JSON value "${Std.string(v)}"');
    }

    return null;
  }

  public static function isArray( v : JsonType ) : Bool {
    return v.match(JTArray(_));
  }

  public static function getArray( v : JsonType ) : Array<JsonType> {
    return switch(v) {
      case JTArray(a):a;
      default: throw new InvalidOperation('not an array');
    }
  }

  public static function isString( v : JsonType ) : Bool {
    return v.match(JTString(_));
  }

  public static function getString( v : JsonType ) : String {
    return switch(v) {
      case JTString(s):s;
      default: throw new InvalidOperation('not a string');
    }
  }

  public static function isObject( v : JsonType ) : Bool {
    return v.match(JTObject(_));
  }

  public static function getObject( v : JsonType ) : StringMap<JsonType> {
    return switch(v) {
      case JTObject(m):m;
      default: throw new InvalidOperation('not an object');
    }
  }

  public static function isDouble( v : JsonType ) : Bool {
    return v.match(JTDouble(_));
  }

  public static function getDouble( v : JsonType ) : Float {
    return switch(v) {
      case JTDouble(f):f;
      case JTInteger(n):n;
      default: throw new InvalidOperation('not a double');
    }
  }

  public static function isNumeric( v : JsonType ) : Bool {
    return switch(v) {
      case JTInteger(_):true;
      case JTDouble(_):true;
      default: false;
    }
  }

  public static function isInteger( v : JsonType ) : Bool {
    return switch(v) {
      case JTInteger(_):true;
      case JTDouble(f): Math.ffloor( f ) == f;
      default: false;
    }
  }

  public static function getInteger( v : JsonType ) : Int {
    return switch(v) {
      case JTInteger(n):n;
      case JTDouble(f): Math.ffloor( f ) == f ? Std.int(f) : throw new InvalidOperation('not an integer');
      default: throw new InvalidOperation('not an integer');
    }
  }

  public static function isBoolean( v : JsonType ) : Bool {
    return v.match(JTBoolean(_));
  }

  public static function getBoolean( v : JsonType ) : Bool {
    return switch(v) {
      case JTBoolean(b):b;
      default: throw new InvalidOperation('not a boolean');
    }
  }

  public static function isNull( v : JsonType ) : Bool {
    return v.match(JTNull);
  }
}

final class UnpackJsonType {
  private function new() {}
  public static function toAny( json : JsonType ) : Any {
    return switch( json ) {
      case JTArray(v): [for( a in v ) toAny( a )];
      case JTString(v): v;
      case JTObject(v): unpackMap(v);
      case JTDouble(v): v;
      case JTInteger(v): v;
      case JTBoolean(v): v;
      case JTNull: null;
      default: throw new UnsupportedOperation('unpack type error');
    }
  }
  static function unpackMap( map : StringMap<JsonType> ) : Any {
    var r = new StringMap<Any>();
    for( k=>v in map.keyValueIterator() ) r.set( k, toAny(v) );
    return r;
  }
  public static function toMap( map : JsonType ) : StringMap<Any> {
    if( ! JsonConverter.isObject(map) ) throw new InvalidOperation('parameter is not a json object');
    var r = new StringMap<Any>();
    for( k=>v in JsonConverter.getObject(map).keyValueIterator() ) r.set( k, toAny(v) );
    return r;
  }
  public static function toArray( array : JsonType ) : Array<Any> {
    if( ! JsonConverter.isArray(array) ) throw new InvalidOperation('parameter is not a json array');
    return [for( v in JsonConverter.getArray( array ) ) toAny( v )];
  }
}
