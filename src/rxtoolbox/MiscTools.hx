/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package rxtoolbox;

import haxe.io.Output;
import haxe.ds.StringMap;
using StringTools;

class MiscTools {
  private function new() {}

  public static function isWindows() : Bool {
    return Sys.systemName() == "Windows";
  }

  public static function println( o: Output, s: Dynamic ) : Void {
    if( null == o ) return;
    o.writeString( Std.string( s ) );
    isWindows() ? o.writeString( '\r\n' ) : o.writeString( '\n' );
  }
  public static function print( o: Output, s: Dynamic ) : Void {
    if( null == o ) return;
    o.writeString( Std.string( s ) );
  }

  public static function formatp( fmt : String, values : Array<Any> ) : String {

    for( idx in 0...values.length ) {
      fmt = fmt.replace( '{$idx}', Std.string( values[ idx ] ) );
    }

    return fmt;
  }

  public static function formatn( fmt : String, values : StringMap<Any> ) : String {

    for( key in values.keys() ) {
      fmt = fmt.replace( '{$key}', Std.string( values.get( key ) ) );
    }

    return fmt;
  }

}
