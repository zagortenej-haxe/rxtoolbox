/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package rxtoolbox;


import haxe.io.Output;
using rxtoolbox.LogTools;

// use implicit conversion 'to Int' and
// operator forwarding for underlying type (Int)
// by NOT defining operator function body
enum abstract LogLevel(Int) to Int {
  var DEBUG;
  var INFO;
  var WARN;
  var ERROR;

  @:op(A > B)
  public function gt( rhs: Int ): Bool;/*  {
    return this > rhs;
  } */

  @:op(A >= B)
  public function ge( rhs: Int ): Bool;/*  {
    return this >= rhs;
  } */

  @:op(A == B)
  public function eq( rhs: Int ): Bool;/*  {
    return this == rhs;
  } */

  @:op(A <= B)
  public function le( rhs: Int ): Bool;/*  {
    return this <= rhs;
  } */

  @:op(A < B)
  public function lt( rhs: Int ): Bool;/*  {
    return this < rhs;
  } */

  // @:to
  // public function toInt() : Int {
  //   return cast this;
  // }
}

interface BasicLogger {
  function debug( msg : String ) : Void;
  function info( msg : String ) : Void;
  function warn( msg : String ) : Void;
  function error( msg : String ) : Void;
  function message( msg : String ) : Void;

  var isDebug(get,never) : Bool;
  function lock() : Void;
  function unlock() : Void;
}

class LogBase implements BasicLogger {

  public var level = LogLevel.INFO;
  var log : Output;
  public var isDebug(get,never) : Bool;

  public function new( output : Output ) {
    this.log = output;
  }

  public function debug( msg : String ) {
    if( level > LogLevel.DEBUG ) return;
    log.debug( msg );
    log.flush();
  }

  public function info( msg : String ) {
    if( level > LogLevel.INFO ) return;
    log.info( msg );
    log.flush();
  }

  public function warn( msg : String ) {
    if( level > LogLevel.WARN ) return;
    log.warn( msg );
    log.flush();
  }

  public function error( msg : String ) {
    // if( level > LogLevel.ERROR ) return;
    log.error( msg );
    log.flush();
  }

  public function message( msg : String ) {
    log.message( msg );
    log.flush();
  }

  function get_isDebug():Bool {
    return level == DEBUG;
  }

  public function lock() : Void {

  }

  public function unlock() : Void {

  }
}
