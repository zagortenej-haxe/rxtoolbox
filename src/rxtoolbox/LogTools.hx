/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package rxtoolbox;

import haxe.io.Output;

using StringTools;
using rxtoolbox.MiscTools;

class LogTools {
  private function new() {}

  public static function debug( log : Output, msg : String ) {
    log.println( "[{0}] [DEBUG] {1}".formatp([timestamp(),msg]) );
  }

  public static function info( log : Output, msg : String ) {
    log.println( "[{0}] [INFO] {1}".formatp([timestamp(),msg]) );
  }

  public static function warn( log : Output, msg : String ) {
    log.println( "[{0}] [WARN] {1}".formatp([timestamp(),msg]) );
  }

  public static function error( log : Output, msg : String ) {
    log.println( "[{0}] [ERROR] {1}".formatp([timestamp(),msg]) );
  }

  public static function message( log : Output, msg : String ) {
    log.println( "[{0}] {1}".formatp([timestamp(),msg]) );
  }

  private static function timestamp() {
    // return Date.now().toString();
    return DateTools.format( Date.now(), "%Y-%m-%d %H:%M:%S" );
  }
}
