/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package rxtoolbox.cli;

import rxtoolbox.json.JsonConverter;
import rxtoolbox.json.JsonConverter.JsonType;
import haxe.io.Eof;
import haxe.io.Input;
import haxe.Json;
import sys.io.Process;

using StringTools;

typedef ProcRunResult = {
    var status: Int;
    var ?text: Array<String>;
    var ?json: JsonType;
}

final class ProcRun {
    private function new(){}

    public static function cmd( cmd : String, ?args : Array<String>, waitForIt : Bool = true, appendStdErr : Bool = false ) : ProcRunResult {
        // trace( cmd + (null != args ? (' ' + args.join(' ')) : '') );

        var pc = new Process( cmd, args );
        if( ! waitForIt ) {
            pc.close();
            return null;
        }

        var result : ProcRunResult = {
            status: pc.exitCode(),
        };
        // trace('$? = ${result.status}');

        if( 0 != result.status ) {

            result.text = collect( pc.stderr );

        } else {

            result.text = collect( pc.stdout );
            if( appendStdErr ) {
                result.text = result.text.concat( collect( pc.stderr ) );
            }
            var content = result.text.join('\n');
            var json : Dynamic = null;
            try { json = Json.parse( content ); } catch(e){}
            if( null != json ) result.json = JsonConverter.convert( json );
        }

        pc.close();

        return result;
    }

    static function collect( input : Input ) : Array<String> {
        var r = [];
        while(true) {
            try {
                r.push(input.readLine());
            } catch(ex : Eof) {
                break;
            }
        }
        return r;
    }
}