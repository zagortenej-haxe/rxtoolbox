/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package rxtoolbox.cli;

import sys.io.Process;
import sys.thread.Mutex;
import haxe.io.Output;
import rxtoolbox.LogBase.LogLevel;
import rxtoolbox.LogBase.BasicLogger;

using rxtoolbox.LogTools;
using rxtoolbox.MiscTools;

class TerminalLog implements BasicLogger {

  // https://en.wikipedia.org/wiki/ANSI_escape_code#CSI_(Control_Sequence_Introducer)_sequences
  // https://en.wikipedia.org/wiki/ANSI_escape_code#SGR
  // https://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html
  // https://gist.github.com/dominikwilkowski/60eed2ea722183769d586c76f22098dd
  public static inline var BLACK = "\x1b[30m";
  public static inline var BLACK_BRIGHT = "\x1b[30;1m";
  public static inline var RED = "\x1b[31m";
  public static inline var RED_BRIGHT = "\x1b[31;1m";
  public static inline var GREEN = "\x1b[32m";
  public static inline var GREEN_BRIGHT = "\x1b[32;1m";
  public static inline var YELLOW = "\x1b[33m";
  public static inline var YELLOW_BRIGHT = "\x1b[33;1m";
  public static inline var BLUE = "\x1b[34m";
  public static inline var BLUE_BRIGHT = "\x1b[34;1m";
  public static inline var MAGENTA = "\x1b[35m";
  public static inline var MAGENTA_BRIGHT = "\x1b[35;1m";
  public static inline var CYAN = "\x1b[36m";
  public static inline var CYAN_BRIGHT = "\x1b[36;1m";
  public static inline var WHITE = "\x1b[37m";
  public static inline var WHITE_BRIGHT = "\x1b[37;1m";
  public static inline var RESET = "\x1b[0m";
  public static inline var BOLD = "\x1b[1m";
  public static inline var ITALIC = "\x1b[3m";

  private static var COLOR_CODE_REGEX : EReg = ~/\x1b\[[^m]+m/g;

  private function maybeStripColour( msg : String ) : String {
    return hasColours ? msg : COLOR_CODE_REGEX.replace( msg, '' );
  }

  public var level = LogLevel.INFO;
  public var isDebug(get,never) : Bool;

  var log : Output;
  var errlog : Output;
  var mutex : Mutex;

  var hasColours : Bool = false;

  public function new() {
    this.log = Sys.stdout();
    this.errlog = Sys.stderr();
    mutex = new Mutex();
  
    if( MiscTools.isWindows() ) {
      hasColours = ( Sys.getEnv('TERM') == 'xterm' || Sys.getEnv( 'ANSICON' ) != null );
    } else {
      try {
        var p = new Process( 'tput', ['colors'] );
        var r = p.exitCode();
        hasColours = ( 0 == r );
        p.close();
      } catch (e:Dynamic) {};
    }
  }

  private function println( msg : String ) {
    log.println( maybeStripColour( msg ) );
    log.flush();
  }

  public function debug( msg : String ) {
    if( level > LogLevel.DEBUG ) return;
    println( YELLOW + '[DEBUG]' + RESET + ' ' + BOLD + msg + RESET );
  }

  public function info( msg : String ) {
    if( level > LogLevel.INFO ) return;
    println( WHITE_BRIGHT + '[INFO]' + RESET + ' ' + BOLD + msg + RESET );
  }

  public function warn( msg : String ) {
    if( level > LogLevel.WARN ) return;
    println( YELLOW_BRIGHT + '[WARNING]' + RESET + ' ' + BOLD + YELLOW + msg + RESET );
  }

  public function error( msg : String ) {
    // if( level > LogLevel.ERROR ) return;
    println( RED_BRIGHT + '[ERROR]' + RESET + ' ' + BOLD + RED + msg + RESET );
  }

  public function message( msg : String ) {
    println( msg );
  }

  function get_isDebug():Bool {
    return level == DEBUG;
  }

  public function lock() : Void {
    mutex.acquire();
  }

  public function unlock() : Void {
    mutex.release();
  }
}