/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package rxtoolbox.cli;

using StringTools;

class ArgsCollector {
  public static final EMPTY_VALUE:String = "";

  public var params:Map<String, String>;
  public var values:Array<String>;

  public function new(args:Array<String>,?switches:Array<String>) {
    params = [];
    values = [];
    switches = switches != null ? switches : [];

    var it = args.iterator();
    while (it.hasNext()) {

      var p = it.next();

      if (p.startsWith("--")) {
        p = p.substr(2);
        var val = EMPTY_VALUE;

        if (!switches.contains(p) && it.hasNext()) {
          val = it.next();
        }

        params.set(p, val);

      } else if (p.startsWith("-")) {

        p = p.substr(1);
        var val = EMPTY_VALUE;

        if (!switches.contains(p) && it.hasNext()) {
          val = it.next();
        }

        params.set(p, val);

      } else {

        values.push(p);
      }
    }
  }

  public function get(param_names:Array<String>):String {
    for (p in param_names) {
      if (this.params.exists(p))
        return this.params.get(p);
    }

    return null;
  }
}
