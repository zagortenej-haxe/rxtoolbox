/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package rxtoolbox.lib;

// https://en.wikipedia.org/wiki/Xorshift
// https://www.jstatsoft.org/article/view/v008i14/xorshift.pdf

class Xorshift32 {

    private static var a : UInt = Std.int( 2463534242.0 );
    // private static var a : UInt = rnd();

    private static function rnd() : UInt {
        // 4294967295 == 2^32 - 1
        return Std.int( Math.random() * 4294967295.0 );
    }

    public static function seed( ?s : UInt ) : Void {
        if( s == null ) {
            s = rnd();
        }
        a = s != 0 ? s : 42;
        trace(a);
    }

    public static function random() : UInt
    {
        var x = a;
        x ^= x << 13;
        x ^= x >> 17;
        x ^= x << 5;
        return a = x;
    }
}
