package rxtoolbox.exception;

import haxe.Exception;

class UnsupportedOperation extends Exception {}
