/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package rxtoolbox.format.resp;

class RespException {
    private var msg : String;

    public function new(msg:String) {
        this.msg = msg;
    }

    public function toString() : String {
        return 'RespException: ${msg}';
    }
}
