/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package rxtoolbox.format.resp;


import haxe.ds.StringMap;
import haxe.io.Bytes;

using StringTools;


/*
  https://redis.io/docs/reference/protocol-spec/#resp-protocol-description

      RESP data type    Minimal protocol    Category    First byte
                        version
  ---+-----------------+-------------------+-----------+----------
   x  Simple strings    RESP2               Simple      +
   x  Simple Errors     RESP2               Simple      -
   x  Integers          RESP2               Simple      :
   x  Bulk strings      RESP2               Aggregate   $
   x  Arrays            RESP2               Aggregate   *
   x  Nulls             RESP3               Simple      _
   x  Booleans          RESP3               Simple      #
   x  Doubles           RESP3               Simple      ,
   o  Big numbers       RESP3               Simple      (
   x  Bulk errors       RESP3               Aggregate   !
   x  Verbatim strings  RESP3               Aggregate   =
   x  Maps              RESP3               Aggregate   %
   o  Sets              RESP3               Aggregate   ~
   o  Pushes            RESP3               Aggregate   >

*/
// https://redis.io/topics/protocol
class Writer {

  private static final EOL = "\r\n";

  var output : haxe.io.Output;
  public var token_count(default, null) : Int;

  public function new( output : haxe.io.Output ) {
      this.output = output;
      token_count = 0;
  }

  public function write_with_token_count( v : Any ) {
    token_count = 0;
    write( v );
  }

  public function write( v : Any ) {

    if( Std.isOfType( v, Int ) ) {
      writeInteger( cast v );
      return;
    }

    if( Std.isOfType( v, String ) ) {
      writeBulkString( Bytes.ofString( cast v ) );
      return;
    }

    if( Std.isOfType( v, Bytes ) ) {
      writeBulkString( cast v );
      return;
    }

    if( Std.isOfType( v, Array ) ) {
      writeArray( cast v );
      return;
    }

    if( v == null ) {
      writeNullValue();
      return;
    }

    if( Std.isOfType( v, Bool ) ) {
      writeBoolean( cast v );
      return;
    }

    if( Std.isOfType( v, Float ) ) {
      writeDouble( cast v );
      return;
    }

    if( Std.isOfType( v, StringMap ) ) {
      writeMap( cast v );
      return;
    }

    writeBulkString( Bytes.ofString( Std.string( v ) ) );
  }

  // https://redis.io/docs/reference/protocol-spec/#simple-strings
  public function writeSimpleString( v : String ) {

    output.writeString( "+" );
    output.writeString( v );
    output.writeString( EOL );

    token_count++;
  }

  // https://redis.io/docs/reference/protocol-spec/#simple-errors
  public function writeError( v : String ) {

    output.writeString( "-" );
    output.writeString( v );
    output.writeString( EOL );

    token_count++;
  }

  // https://redis.io/docs/reference/protocol-spec/#integers
  public function writeInteger( i : Int ) {

    output.writeString( ":" );
    output.writeString( Std.string( i ) );
    output.writeString( EOL );

    token_count++;
  }

  // https://redis.io/docs/reference/protocol-spec/#bulk-strings
  public function writeBulkString( b : Bytes ) {

    output.writeString( "$" );
    output.writeString( Std.string( b.length ) );
    output.writeString( EOL );
    output.writeFullBytes( b, 0, b.length );
    output.writeString( EOL );

    token_count++;
  }

  public function writeNullBulkString() {

    output.writeString( "$" );
    output.writeString( "-1" );
    output.writeString( EOL );

    token_count++;
  }

  // https://redis.io/docs/reference/protocol-spec/#arrays
  public function writeArray( arr : Array<Any> ) {

    output.writeString( "*" );
    output.writeString( Std.string( arr.length ) );
    output.writeString( EOL );

    for( el in arr ) {
      write( el );
    }

    token_count++;
  }

  public function writeNullArray() {

    output.writeString( "*" );
    output.writeString( "-1" );
    output.writeString( EOL );

    token_count++;
  }

  // https://redis.io/docs/reference/protocol-spec/#nulls
  public function writeNullValue() {

    output.writeString( "_" );
    output.writeString( EOL );

    token_count++;
  }

  // https://redis.io/docs/reference/protocol-spec/#booleans
  public function writeBoolean( b : Bool ) {

    output.writeString( "#" );
    output.writeString( b ? "t" : "f" );
    output.writeString( EOL );

    token_count++;
  }

  // https://redis.io/docs/reference/protocol-spec/#doubles
  public function writeDouble( f : Float ) {

    output.writeString( "," );
    output.writeString( Std.string( f ) );
    output.writeString( EOL );

    token_count++;
  }

  // https://redis.io/docs/reference/protocol-spec/#big-numbers
  // public function writeBigNumber( n : Dynamic ) {
  //   output.writeString( "(" );
  //   output.writeString( n );
  //   output.writeString( EOL );
  //   token_count++;
  // }

  // https://redis.io/docs/reference/protocol-spec/#bulk-errors
  public function writeBulkError( b : Bytes ) {

    output.writeString( "!" );
    output.writeString( Std.string( b.length ) );
    output.writeString( EOL );
    output.writeFullBytes( b, 0, b.length );
    output.writeString( EOL );

    token_count++;
  }

  // https://redis.io/docs/reference/protocol-spec/#verbatim-strings
  public function writeVerbatimString( b : Bytes, enc : String = "txt" ) {

    output.writeString( "=" );
    output.writeString( Std.string( b.length ) );
    output.writeString( EOL );
    output.writeString( enc.lpad( " ", 3 ).substr( 0, 3 ) );
    output.writeString( ":" );
    output.writeString( EOL );
    output.writeFullBytes( b, 0, b.length );
    output.writeString( EOL );

    token_count++;
  }

  // https://redis.io/docs/reference/protocol-spec/#maps
  public function writeMap( map : StringMap<Any> ) {

    var it = map.iterator();
    var itemCount = 0;
    while( it.hasNext() ) {
      it.next();
      itemCount++;
    }

    output.writeString( "%" );
    output.writeString( Std.string( itemCount ) );
    output.writeString( EOL );

    for( key => value in map ) {

      writeSimpleString( key );
      write( value );
    }

    token_count++;
  }

  // https://redis.io/docs/reference/protocol-spec/#sets
  // public function writeSet( set : Set<Any> ) {
  //   output.writeString( "~" );
  //   output.writeString( Std.string( set.size ) );
  //   output.writeString( EOL );
  //   for( value in set ) {
  //     write( value );
  //   }
  //   token_count++;
  // }
}
