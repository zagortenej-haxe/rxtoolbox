/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package rxtoolbox.format.resp;

import haxe.io.BytesBuffer;
import haxe.io.Bytes;
import rxtoolbox.format.resp.Data;

// https://redis.io/topics/protocol
class Reader {

    var input : haxe.io.Input;

    public function new( input : haxe.io.Input ) {
        this.input = input;
    }

    public function read() : RespType {

        var type = input.readString(1);

        switch( type ) {

            case "+":
                return parseSimpleString();

            case "-":
                return parseError();

            case ":":
                return parseInteger();

            case ",":
                return parseDouble();

            case "#":
                return parseBoolean();

            case "$":
                return parseBulkString();

            case "*":
                return parseArray();
        }

        var line = readLine();
        return TUnknown( '${type}${line}' );
    }

    public function parseSimpleString() : RespType {

        var line = readLine();

        return TSimpleString(line);
    }

    public function parseError() : RespType {

        var line = readLine();

        return TError(line);
    }

    public function parseBoolean() : RespType {

        var line = readLine();

        var value = switch(line) {
          case "t": true;
          case "f": false;
          default: throw new RespException('"${line}" is not a valid boolean' );
        }

        var b : Bool = value;

        return TBoolean( b );
    }

    public function parseInteger() : RespType {

        var line = readLine();
        var value = Std.parseInt( line );

        if( value == null )
            throw new RespException('"${line}" is not a valid integer' );

        var i : Int = value;

        return TInteger( i );
    }

    public function parseDouble() : RespType {

      var line = readLine();
      var value = Std.parseFloat( line );

      if( value == Math.NaN )
          throw new RespException('"${line}" is not a valid floating point number' );

      var f : Float = value;

      return TDouble( f );
  }

    public function parseBulkString() : RespType {

        var length = switch( parseInteger() ) {
            case TInteger(i): i;
            case _: throw new RespException('Unexpected enum match' );
        }

        if( length == -1 )
            return TNullValue;

        // var data = Bytes.alloc( length );
        // input.readFullBytes( data, 0, length );
        var data = input.read( length );
        // read \r\n
        input.read(2);

        return TBulkString( data );
    }

    public function parseArray() : RespType {

        var length = switch( parseInteger() ) {
            case TInteger(i): i;
            case _: throw new RespException('Unexpected enum match' );
        }

        if( length == -1 )
            return TNullValue;

        var r : Array<RespType> = [];

        while( length > 0 ) {
            r.push( read() );
            length--;
        }

        return TArray( r );
    }

	private function readLine() : String {

		var buf = new BytesBuffer();

        var b;
        while( true ) {
            b = input.readByte();
            if( b == "\n".code ) break;
            buf.addByte( b );
        }

        var s = buf.getBytes().toString();
        if( s.charCodeAt( s.length - 1 ) == "\r".code ) {
            s = s.substr( 0, -1 );
        }

		return s;
	}
}
