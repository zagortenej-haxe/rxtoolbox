/* Copyright 2024 Ravendyne Inc. */
/* SPDX-License-Identifier: MIT */

package rxtoolbox.format.resp;

import haxe.ds.StringMap;
import haxe.io.Bytes;

enum RespType {

    TSimpleString( v : String );
    TError( e : String );
    TInteger( i : Int );
    TDouble( d : Float );
    TBoolean( b : Bool );
    TBulkString( b : Bytes );
    TArray( a : Array<RespType> );
    TMap( m : StringMap<RespType> );

    TNullValue;

    TUnknown( l : String );
}

class Data {

    public static function unpackArray( arr : Array<RespType> ) : Array<Any> {
        var result : Array<Any> = [];

        for( rt in arr ) {
            switch( rt ) {
                case TSimpleString(v): result.push( v );
                case TInteger(i): result.push( i );
                case TDouble(d): result.push( d );
                case TBoolean(b): result.push( b );
                case TNullValue: result.push( null );
                case TBulkString(b): result.push( b );
                case TError(e): result.push( e );
                case TUnknown(l): result.push( l );
                case TArray(a): result.push( unpackArray( a ) );
                case TMap(m): result.push( unpackMap( m ) );
            }
        }

        return result;
    }

    public static function unpackMap( map : StringMap<RespType> ) : StringMap<Any> {
        var result = new StringMap<Any>();

        for( key => value in map.keyValueIterator() ) {
            switch( value ) {
                case TSimpleString(v): result.set( key, v );
                case TInteger(i): result.set( key, i );
                case TDouble(d): result.set( key, d );
                case TBoolean(b): result.set( key, b );
                case TNullValue: result.set( key, null );
                case TBulkString(b): result.set( key, b );
                case TError(e): result.set( key, e );
                case TUnknown(l): result.set( key, l );
                case TArray(a): result.set( key, unpackArray( a ) );
                case TMap(m): result.set( key, unpackMap( m ) );
            }
        }

        return result;
    }

    public static function convertArrayBytesToString( arr: Array<Any> ) : Array<Any> {
        var result : Array<Any> = [];

        for( e in arr ) {
            if( Std.isOfType( e, Bytes ) )
                result.push( Std.string( e ) );
            else
                result.push( e );
        }

        return result;
    }
}
