package rxtoolbox.rxhx;

typedef ConsumerHandlers<T> = {
  var ?next : T -> Void;
  var ?error : String -> Void;
  var ?complete : Void -> Void;
}

class Consumer<T> {
  var handlers : ConsumerHandlers<T>;
  var isUnsubscribed : Bool;
  public var onUnsubscribe : Void -> Void;

  public function new( handlers : ConsumerHandlers<T> ) {
    this.handlers = handlers; // next, error and complete logic
    isUnsubscribed = false;
  }

  public function next( ?value : T ) {
    if( null != handlers.next && ! isUnsubscribed ) {
      handlers.next( value );
    }
  }

  public function error( error : String ) {
    if( ! isUnsubscribed ) {
      if( null != handlers.error ) {
        handlers.error( error );
      }
        
      unsubscribe();
    }
  }

  public function complete() {
    if( ! isUnsubscribed ) {
      if( null != handlers.complete ) {
        handlers.complete();
      }

      unsubscribe();
    }
  }

  public function unsubscribe() {
    if( isUnsubscribed ) return;
    isUnsubscribed = true;

    // call any cancellation logic,
    // useful for async producers
    if( null != onUnsubscribe ) {
      onUnsubscribe();
    }
  }
}
