package rxtoolbox.rxhx;

import haxe.Timer;
import rxtoolbox.rxhx.Consumer.ConsumerHandlers;

typedef ProducerProduction<T> = Consumer<T> -> Void;

class Producer<T> {
  // subscribe() param is function that accepts consumer
  // and returns a cancellation logic which is called when
  // consumer unsubscribes
  var producer : ProducerProduction<T>;
  var onUnsubscribe : Void -> Void;

  public var isSubscribed(default,null) : Bool;

  public function new( producer : ProducerProduction<T>, ?onUnsubscribe : Void -> Void ) {
    this.producer = producer;
    this.onUnsubscribe = onUnsubscribe;
    isSubscribed = false;
  }

  public function subscribe( handlers : ConsumerHandlers<T> ) : Subscription<T> {
    final consumer = new Consumer<T>( handlers );

    // producer() executes producer's value production logic.
    // If producer is synchronuous, this will be synchronuous execution,
    // if it is async, this will execute producer's logic 
    // which should start up the value production and return a cancellation handler.
    // Consumer's next(), error() and complete()
    // will be called asynchronuously depending on when producer calls them.
    // The returned cancellation handler function will be called when
    // subscriber calls unsubscribe() method on returned Subscription.
    consumer.onUnsubscribe = onUnsubscribe;
    isSubscribed = true;
    #if sys
    // trace('MAKE!!!');
    try {
      sys.thread.Thread.current().events.run(() -> {
        // trace('RUN!!!');
        this.producer( consumer );
      });
    } catch( ex : sys.thread.NoEventLoopException ) {
      // trace('EXCEPTION!');
      sys.thread.Thread.create(() -> {
        // trace('RUN!!!');
        this.producer( consumer );
      });
      // sys.thread.Thread.runWithEventLoop(() -> {
      //   trace('RUN!!!');
      //   this.producer( consumer );
      // });
    }
    #else
    haxe.Timer.delay(() -> {
      this.producer( consumer );
    },0);
    #end

    // subscription is just mediator so we can call unsubscribe()
    return new Subscription<T>( consumer );
  }

  public static function of<T>( value : T ) : Producer<T> {
    return new Producer<T>( consumer -> {
      consumer.next( value );
      consumer.complete();
    });
  }

  public static function from<T>( values : Array<T> ) : Producer<T> {
    return new Producer<T>( consumer -> {
      for( v in values ) consumer.next( v );
      consumer.complete();
    });
  }

  public static function empty<T>() : Producer<T> {
    return new Producer<T>( consumer -> {
      consumer.next();
      consumer.complete();
    });
  }

  public static function interval<T>( timeMs : Int, v : () -> T ) : Producer<T> {
    // https://api.haxe.org/haxe/Timer.html
    var timer = new Timer( timeMs );
    return new Producer<T>( consumer -> {
      timer.run = () -> {
        consumer.next( v() );
      }
    }, () -> {
      timer.stop();
    });
  }
}
