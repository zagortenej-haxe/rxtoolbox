package rxtoolbox.rxhx;

class Subscription<T> {
    var consumer : Consumer<T>;

    public function new( consumer : Consumer<T> ) {
        this.consumer = consumer;
    }

    public function unsubscribe() {
        consumer.unsubscribe();
    }
}
