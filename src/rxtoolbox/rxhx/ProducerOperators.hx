package rxtoolbox.rxhx;

import rxtoolbox.rxhx.Consumer.ConsumerHandlers;
import haxe.Timer;

typedef Ref<T> = {
  var val : T;
}

class ProducerOperators {

  public static function delay<T>( me: Producer<T>, timeMs : Int ) : Producer<T> {
    if( me.isSubscribed ) return null;

    var timer : Timer = null;
    var subscription = null;

    return new Producer<T>( consumer -> {

      final trigger_delay = function(value:T) {
        // https://api.haxe.org/haxe/Timer.html
        timer = new Timer( timeMs );
        timer.run = () -> {
          timer.stop();
          consumer.next( value );
          consumer.complete();
        }
      }

      subscription = me.subscribe({
        next: value -> trigger_delay( value ),
        error: err -> consumer.error( err ),
        // we ignore complete() from "me" because it
        // may come before our delay timeout, we will
        // complete() once we emit the value
      });
    }, () -> {
      if( null != timer ) timer.stop();
      subscription.unsubscribe();
    });
  }

  public static function map<T,R>( me: Producer<T>, transformation : T -> R ) : Producer<R> {
    if( me.isSubscribed ) return null;

    var subscription;

    return new Producer<R>( consumer -> {
      subscription = me.subscribe({
        next: value -> consumer.next( transformation( value ) ),
        error: err -> consumer.error( err ),
        complete: () -> consumer.complete(),
      });
    }, () -> subscription.unsubscribe() );
  }

  public static function switchMap<T,R>( me: Producer<T>, projection : T -> Producer<R> ) : Producer<R> {
    if( me.isSubscribed ) return null;

    var subscription = null;
    var inner_subscriptions = [];
    var me_next_count = 0;

    return new Producer<R>( consumer -> {
      subscription = me.subscribe({
        next: value -> {
          // trace('me next', value);
          inner_subscriptions.push( projection( value ).subscribe({
            next: v -> {
              // trace('inner next', v);
              consumer.next( v );
            },
            error: err -> consumer.error( err ),
            complete: () -> {
              // trace('inner complete');
              me_next_count--;
              if( me_next_count <= 0 ) {
                // trace('inner complete complete');
                consumer.complete();
              }
            }
          }) );
          me_next_count++;
        },
        error: err -> consumer.error( err ),
        complete: () -> {
          // trace('me complete');
        },
      });
    }, () -> {
      // trace('on-unsubscribe');
      subscription.unsubscribe();
      for( isu in inner_subscriptions) isu.unsubscribe();
    } );
  }

  public static function take<T>( me: Producer<T>, count : Int ) : Producer<T> {
    if( me.isSubscribed ) return null;

    if( count <= 0 ) return Producer.empty();

    var subscription : Subscription<T> = null;

    return new Producer<T>( consumer -> {
      var taken : Int = 0;

      final production : ConsumerHandlers<T> = {
        next: value -> {
          taken++;
          consumer.next( value );
          if( taken < count ) {
          } else {
            subscription.unsubscribe();
            consumer.complete();
          }
        },
        error: err -> consumer.error( err ),
        complete: () -> consumer.complete(),
      };

      subscription = me.subscribe( production );
    }, () -> subscription.unsubscribe() );
  }

  public static function collect<T>( me: Producer<T> ) : Producer<Array<T>> {
    if( me.isSubscribed ) return null;

    var subscription : Subscription<T> = null;

    return new Producer<Array<T>>( consumer -> {
      var taken : Array<T> = [];

      final production : ConsumerHandlers<T> = {
        next: value -> {
          taken.push( value );
        },
        error: err -> consumer.error( err ),
        complete: () -> {
          subscription.unsubscribe();
          consumer.next( taken );
          consumer.complete();
        },
      };

      subscription = me.subscribe( production );
    }, () -> subscription.unsubscribe() );
  }
}
