#!/bin/bash

set -e

haxe build.hxml

neko build/rxtools.n --in
echo "-------"
neko build/rxtools.n --in input/file.txt
echo "-------"
neko build/rxtools.n --in input/file.txt --flag-foo
echo "-------"
neko build/rxtools.n --in --flag-foo
echo "-------"
neko build/rxtools.n --in abc --json

echo "-------"
neko build/rxtools.n --in abc --obs

echo "-------"
neko build/rxtools.n --in abc --evt
